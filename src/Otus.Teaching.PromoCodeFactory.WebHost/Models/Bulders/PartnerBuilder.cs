﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Bulders
{
    public class PartnerBuilder
    {
        private Partner _partner;
        public PartnerBuilder()
        {
            _partner = new Partner();
        }

        public PartnerBuilder SetNumberIssuedPromoCodes(int NumberIssuedPromoCodes)
        {
            _partner.NumberIssuedPromoCodes = NumberIssuedPromoCodes;
            return this;
        }

        public Partner Build()
        {
            return _partner;
        }
    }
}
