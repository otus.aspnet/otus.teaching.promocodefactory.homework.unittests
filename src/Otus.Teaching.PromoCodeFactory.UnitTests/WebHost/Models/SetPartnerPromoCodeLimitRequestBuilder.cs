﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Models
{
    public class SetPartnerPromoCodeLimitRequestBuilder
    {
        private SetPartnerPromoCodeLimitRequest _request;
        public SetPartnerPromoCodeLimitRequestBuilder()
        {
            _request = new SetPartnerPromoCodeLimitRequest();
        }

        public SetPartnerPromoCodeLimitRequestBuilder SetLimit(int limit)
        {
            _request.Limit = limit;
            return this;
        }
        public SetPartnerPromoCodeLimitRequest Build()
        {
            return _request;
        }

    }
}
