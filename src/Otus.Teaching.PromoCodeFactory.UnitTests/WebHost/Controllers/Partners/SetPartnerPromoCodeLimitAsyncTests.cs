﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_FoundPartner_NotFound()
        {
            var req = new SetPartnerPromoCodeLimitRequestBuilder()
                .Build();

            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            var actionResult = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, req);

            //Assert
            actionResult.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var req = new SetPartnerPromoCodeLimitRequestBuilder()
                .Build();

            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = new Partner();
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, req);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется. Нужно отключить предыдущий лимит
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_FoundActiveLimit()
        {
            // Arrange
            var req = new SetPartnerPromoCodeLimitRequestBuilder()
                .SetLimit(1)
                .Build();

            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = 1;

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, req);


            // Assert
            partner.NumberIssuedPromoCodes.Should().Equals(0);
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                x.CancelDate.HasValue);
            activeLimit.Should().NotBeNull();
        }

        /// <summary>
        /// Лимит должен быть больше 0
        /// </summary>
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitAsync_SmallNewLimit_BadRequest(int limit)
        {
            var req = new SetPartnerPromoCodeLimitRequestBuilder()
                .SetLimit(limit)
                .Build();
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = 1;

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, req);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();

        }

        /// <summary>
        /// Вызов update для сущности partner.
        /// </summary>
        /// <param name="limit"></param>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_CallUpdate_BadRequest()
        {
            var req = new SetPartnerPromoCodeLimitRequestBuilder()
                .SetLimit(1)
                .Build();

            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = 1;

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, req);

            // Assert
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once());
        }
    }
}